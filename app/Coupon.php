<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Coupon extends Model
{

    protected $connection = 'mysql';

    protected $fillable = [
        'barcode', 'name',  'offer', 'image_id',  'date_start', 'date_finish',
    ];


}
