<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Http\Requests;
use App\User;
use App\UserCoupon;
use Illuminate\Http\Request;
use Validator;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function showCoupon($id)
    {

        return response()->json(Coupon::find($id));
        //
    }


    public function coupons(Request $request)
    {
        $input = $request->all();


        $rules = [
            'coupons.*.barcode' => 'required|digits:13',
            'coupons.*.name' => 'required|max:100',
            'coupons.*.offer' => 'required|max:10',
            'user_coupons.*.login' => 'required|digits:11',
            'user_coupons.*.coupons.*.barcode' => 'required|digits:13',
        ];


        $validator = Validator::make($input['data'], $rules);
        if ($validator->passes()) {

        } else {

            return response()->json($validator->errors()->all(), 400);

        }


        $data = $input['data'];

        $coupons = $data['coupons'];

        $user_coupons = $data['user_coupons'];

        //order from smaller to bigger

        $loginsort = array_column($user_coupons, 'login');

        array_multisort($loginsort, SORT_DESC, $user_coupons);


        //first thing first test all api, then create. ALL OR NOTHING
        foreach ($coupons as $coupon):
            $isset_coupon = Coupon::where('barcode', $coupon['barcode'])->first();
            if (!is_null($isset_coupon)) {
                return response()->json('Coupon already exist ' . $isset_coupon, 500);
            }
        endforeach;


        foreach ($user_coupons as $user_coupon):
            $login = $user_coupon['login'];
            $user = User::where('login', $login)->first();
            if (is_null($user)) {
                return response()->json('User ' . $login . ' do not exist', 500);
            }
            $coupons = $user_coupon['coupons'];
            foreach ($coupons as $coupon):

                $barcode = $coupon['barcode'];
                $isset_coupon = UserCoupon::where('barcode', $barcode)->where('login', $login)->first();
                if (!is_null($isset_coupon)) {
                    return response()->json('Coupon already exist ' . $isset_coupon, 500);
                }
            endforeach;
        endforeach;


        //after test work

        foreach ($coupons as $coupon):

            Coupon::create($coupon);
        endforeach;

        foreach ($user_coupons as $user_coupon):
            $login = $user_coupon['login'];

            $coupons = $user_coupon['coupons'];
            foreach ($coupons as $coupon):
                $barcode = $coupon['barcode'];

                UserCoupon::create([
                    'barcode' => $barcode,
                    'login' => $login,
                    'active' => $coupon['active']
                ]);

            endforeach;
        endforeach;

        return response()->json($input, 201);
        //
    }


    //
}
