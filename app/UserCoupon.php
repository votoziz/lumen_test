<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class UserCoupon extends Model
{

    protected $fillable = [
        'barcode', 'login',  'active'
    ];


}
