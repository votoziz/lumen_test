<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'login' => '79'.$faker->unique()->randomNumber($nbDigits = 9, $strict = true)
    ];
});


$factory->define(App\Coupon::class, function (Faker\Generator $faker) {
    return [
        'barcode' => $faker->unique()->ean13(),
        'name' => $faker->text( 100),
        'offer' => $faker->text( 10),
        'date_start'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'date_finish'=>$faker->date($format = 'Y-m-d', $max = 'now'),
    ];
});


$factory->define(App\UserCoupon::class, function ($faker) {
    return [
        'active' => 1,
        'barcode' => function () {
            return factory(App\Coupon::class)->create()->barcode;
        },
        'login' => function () {
            return factory(App\User::class)->create()->login;
        }
    ];
});


