<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->float('login', 11, 0);
            $table->index(['login']);
            $table->timestamps();
        });


        DB::table('users')->insert([
            'login' => '79524909752',

        ]);


        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->float('barcode', 13, 0);
            $table->string('name',100);
            $table->string('offer',20);

            $table->integer ( 'image_id' )->unsigned()->nullable();
//            $table->foreign ( 'image_id' )->references ( 'id' )->on ( 'images' ) ->onUpdate('cascade')->onDelete('cascade');
            $table->date('date_start');
            $table->date('date_finish');

            $table->index(['barcode']);

            $table->timestamps();
        });

        DB::table('coupons')->insert([
            'barcode' => '5555555555555',
            'name' => 'primitive test',
            'offer' => '50%',
            'date_start'=>'2019-06-18',
            'date_finish'=>'2019-06-20'
        ]);




        Schema::create('user_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->float('barcode', 13, 0);
            $table->foreign ( 'barcode' )->references ( 'barcode' )->on ( 'coupons' ) ->onUpdate('cascade')->onDelete('cascade');

            $table->float ( 'login' ,11,0);
            $table->foreign ( 'login' )->references ( 'login' )->on ( 'users' ) ->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('active');

            $table->timestamps();
        });

        DB::table('user_coupons')->insert([
            'barcode' => '5555555555555',
            'login' => '79524909752',
            'active' => true,

        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {

        Schema::dropIfExists('user_coupons');
        Schema::dropIfExists('coupons');
        Schema::dropIfExists('users');
    }
}
